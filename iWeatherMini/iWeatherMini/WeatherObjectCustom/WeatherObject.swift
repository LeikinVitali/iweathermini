
import Foundation

class WeatherObject: Codable{
    
    var main: WeatherObjectMain?
    var id: Int?
    var name: String?
    var weather:[WeatherObjectIcon]?
    
}
