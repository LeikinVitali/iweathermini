
import Foundation

class WeatherObjectJson: Codable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}
