
import Foundation

class CurrentObjectJson: Codable{
    
    var dt: Date?
    var temp: Double?
    var weather:[WeatherObjectJson]?
}
