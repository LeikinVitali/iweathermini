

import Foundation
import UIKit



class SecondViewModel: NSObject {
    
// MARK: - let
    let backgroundImageView: Bindable<UIImage> = Bindable(UIImage())
    let currentCityLabel: Bindable<String> = Bindable("")
    let currentDescriptionWeatherLabel: Bindable<String> = Bindable("")
    let tempBasicLabel: Bindable<String> = Bindable("")
    let currentImageView: Bindable<UIImage> = Bindable(UIImage())
    
    // MARK: - var

    var weatherLoadSecond:WeatherObject?

    // MARK: - func
    
    func callLoadDataUrl(city: String){
       guard let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=ebce8b38958b3796769efaf400482dad&lang=ru&units=metric") else {
           return
       }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { [self] (data, response, error) in
            
            if error == nil, let data = data{
                
                do {
                    let data = try JSONDecoder().decode(WeatherObject.self, from: data)
                    print(data)
                    self.weatherLoadSecond = data
                    print(weatherLoadSecond?.name as Any)
                    self.printStoreboard()
                } catch {
                    print(error)
                }
                } else {
                    print(error?.localizedDescription as Any)
                }

        }
        task.resume()
        
        
    }
    
    
    
    func printStoreboard(){
        
        DispatchQueue.main.async {
            
            if self.weatherLoadSecond?.name == nil {
                self.currentCityLabel.value = "Неправельно указан город"
                return
            }else{
            
            self.tempBasicLabel.value = "\(String(format: "%.1f",(self.weatherLoadSecond?.main?.temp ?? 0.0))) °C"
            self.currentDescriptionWeatherLabel.value = (self.weatherLoadSecond?.weather?.first?.description) ?? ""
           
            self.currentCityLabel.value = self.weatherLoadSecond?.name ?? " "
            self.backgroundImageView.value = UIImage(named: self.weatherLoadSecond?.weather?.first?.icon ?? "01d") ?? UIImage()
            let url = URL(string: "https://openweathermap.org/img/wn/\(self.weatherLoadSecond?.weather?.first?.icon ?? "")@2x.png")
            if let data = try? Data(contentsOf: url!)
                {
                    self.currentImageView.value = UIImage(data: data)!
                }
        }
    }
    
    }
    
}
