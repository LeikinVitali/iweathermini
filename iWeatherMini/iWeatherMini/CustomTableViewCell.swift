

import UIKit
import Kingfisher

class CustomTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dayDateCellLabel: UILabel!
    @IBOutlet weak var dayImageViewCell: UIImageView!
    @IBOutlet weak var dayTempCellLabel: UILabel!
    @IBOutlet weak var nightDateCellLabel: UILabel!
    @IBOutlet weak var nightImageViewCell: UIImageView!
    @IBOutlet weak var nightTempCellLabel: UILabel!
    
    
    
    func configureCell(with object: DailyObjectJson ){
        
        self.dayTempCellLabel.text = " \(String(format: "%.1f", object.temp?.day ?? 0.0)) °C"
        self.dayDateCellLabel.text = " \(getDayForDate(object.dt))"
        
        let dayUrl = URL(string: "https://openweathermap.org/img/wn/\(object.weather?.first?.icon ?? "")@2x.png")
        self.dayImageViewCell.kf.setImage(with: dayUrl)
        
        self.nightTempCellLabel.text = " \(String(format: "%.1f",object.temp?.night ?? 0.0)) °C"
        self.nightDateCellLabel.text = " \(getDayForDate(object.dt))"
        
        self.nightImageViewCell.kf.setImage(with:  loadImageUrlNight(object.weather?.first?.icon ?? " "))
        
        
    }
    
    
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM"
        return formatter.string(from: inputDate)
    }
    
    
    
    
    func loadImageUrlNight(_ str: String) -> URL{
        switch str {
        case "01d":
            let url = URL(string: "https://openweathermap.org/img/wn/01n@2x.png")
            return url!
        case "02d":
            let url = URL(string: "https://openweathermap.org/img/wn/02n@2x.png")
            return url!
        case "03d":
            let url = URL(string: "https://openweathermap.org/img/wn/03n@2x.png")
            return url!
        case "04d":
            let url = URL(string: "https://openweathermap.org/img/wn/04n@2x.png")
            return url!
        case "09d":
            let url = URL(string: "https://openweathermap.org/img/wn/09n@2x.png")
            return url!
        case "10d":
            let url = URL(string: "https://openweathermap.org/img/wn/10n@2x.png")
            return url!
        case "11d":
            let url = URL(string: "https://openweathermap.org/img/wn/11n@2x.png")
            return url!
        case "13d":
            let url = URL(string: "https://openweathermap.org/img/wn/13n@2x.png")
            return url!
        case "50d":
            let url = URL(string: "https://openweathermap.org/img/wn/50n@2x.png")
            return url!
            
        default:
            let url = URL(string: "https://openweathermap.org/img/wn/10n@.png")
            return url!
            
        }
    }
    
    
    
    
    
}
