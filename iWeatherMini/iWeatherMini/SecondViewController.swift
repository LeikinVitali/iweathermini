

import UIKit

class SecondViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var currentCityLabel: UILabel!
    @IBOutlet weak var currentDescriptionWeatherLabel: UILabel!
    @IBOutlet weak var tempBasicLabel: UILabel!
    @IBOutlet weak var currentImageView: UIImageView!
    
    // MARK: - var
    
    private var secondViewModel = SecondViewModel()
    
    // MARK: - viewDidLoad
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.swipeViewController()
        self.configureUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    
    // MARK: - func
    
    private func configureUI(){
        
        self.secondViewModel.tempBasicLabel.bind { (tempBasicLabel) in
            self.tempBasicLabel.text = tempBasicLabel
        }
        
        self.secondViewModel.currentDescriptionWeatherLabel.bind { (currentDescriptionWeatherLabel) in
            self.currentDescriptionWeatherLabel.text = currentDescriptionWeatherLabel
        }
        
        self.secondViewModel.currentCityLabel.bind { (currentCityLabel) in
            self.currentCityLabel.text = currentCityLabel
        }
        
        self.secondViewModel.currentImageView.bind { (currentImageView) in
            self.currentImageView.image = currentImageView
        }
        
        self.secondViewModel.backgroundImageView.bind { (backgroundImageView) in
            self.backgroundImageView.image = backgroundImageView
        }
        
    }
    
    
    
    
    
    func swipeViewController(){
        
        let swipeClear = UISwipeGestureRecognizer(target: self, action: #selector(jumpViewController))
        swipeClear.direction = .right
        self.view.addGestureRecognizer(swipeClear)
        
    }
    
    
    @objc func jumpViewController(){
        
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    // MARK: - IBAction
    
    @IBAction func searchButton(_ sender: UIButton) {
        
        if self.textFieldSearch.text == "" {
            return
        } else{
            guard var city = self.textFieldSearch.text else { return }
            city = city.replacingOccurrences(of: " ", with: "%20")
            
            self.secondViewModel.callLoadDataUrl(city: city)
        }
        
    }
    
    
    
}
