
import UIKit
import CoreLocation
import Kingfisher

class ViewController: UIViewController, CLLocationManagerDelegate {
    // MARK: - IBOutlet
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UIView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var currentCityLabel: UILabel!
    @IBOutlet weak var currentTempBasicLabel: UILabel!
    @IBOutlet weak var currentImageView: UIImageView!
    @IBOutlet weak var currentDescriptionWeatherLabel: UILabel!
    @IBOutlet weak var currentDateLabel: UILabel!
    
    // MARK: - let
    private let locationManager = CLLocationManager()
    
    // MARK: - var
    
    var weatherLoad:WheaterObjectJson?
    var models = [DailyObjectJson]()
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.swipeSecondViewController()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startlocationManager()
    }
    
    
    // MARK: - func
    
    
    func startlocationManager() {
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations:[CLLocation]){
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        print("location = \(location.latitude) \(location.longitude)")
        self.locationManager.stopUpdatingLocation()
        let lat = String(location.latitude)
        let long = String(location.longitude)
        self.callLoadDataUrl(lat: lat, long: long)
        
    }
    
    
    func callLoadDataUrl(lat: String, long: String){
        
        
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(lat)&lon=\(long)&exclude=minutely,hourly&units=metric&lang=ru&appid=ebce8b38958b3796769efaf400482dad") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error == nil, let data = data{
                
                do {
                    let data = try JSONDecoder().decode(WheaterObjectJson.self, from: data)
                    print(data)
                    self.weatherLoad = data
                    self.printStoreboard()
                    
                    
                    let entries = self.weatherLoad?.daily
                    self.models.append(contentsOf: entries!)
                    
                    DispatchQueue.main.async {
                        self.myTableView.refreshControl?.endRefreshing()
                        self.myTableView.reloadData()
                    }
                } catch {
                    print(error)
                }
            } else {
                print(error?.localizedDescription as Any)
            }
            
        }
        task.resume()
        
        
    }
    
    
    
    
    
    
    func printStoreboard(){
        DispatchQueue.main.async { [self] in
            self.currentTempBasicLabel.text = "\(String(format: "%.1f",(self.weatherLoad?.current?.temp ?? 0.0))) °C"
            self.currentDescriptionWeatherLabel.text = self.weatherLoad?.current?.weather?.first?.description
            
            self.currentDateLabel.text = self.createDate((self.weatherLoad?.current?.dt)!)
            self.currentCityLabel.text = self.weatherLoad?.timezone
            self.backgroundImageView.image = UIImage(named: self.weatherLoad?.current?.weather?.first?.icon ?? " ")
            
            let url = URL(string: "https://openweathermap.org/img/wn/\(self.weatherLoad?.current?.weather?.first?.icon ?? "")@2x.png")
            self.currentImageView.kf.setImage(with: url)
            
        }
    }
    
    
    
    
    func createDate(_ date: Date?) -> String{
        
        guard let inputDate = date else {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM"
        return formatter.string(from: inputDate)
    }
    
    
    
    func swipeSecondViewController(){
        
        let swipeClear = UISwipeGestureRecognizer(target: self, action: #selector(jumpSecondViewController))
        swipeClear.direction = .left
        self.view.addGestureRecognizer(swipeClear)
        
    }
    
    
    @objc func jumpSecondViewController(){
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController else{return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    
    
}



extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return models.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else{
            return UITableViewCell()
        }
        
        
        
        
        
        DispatchQueue.main.async {
            
            cell.configureCell(with: self.models[indexPath.row])
            
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
        
    }
    
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM"
        return formatter.string(from: inputDate)
    }
    
    
    
}
